import { IsNotEmpty } from 'class-validator';

export class CreateTaskDto {
  @IsNotEmpty({
    message: 'title tidak boleh kosong',
  })
  title: string;

  @IsNotEmpty()
  description: string;
}
