export interface JwtPayload {
  sub: String;
  username: string;
}
