import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserCredentialsDto } from './dto/user-credentials.dto';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from './guard/jwt-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  signUp(@Body() userCredentialsDto: UserCredentialsDto): Promise<void> {
    return this.authService.createUser(userCredentialsDto);
  }

  @Post('/signin')
  signIn(
    @Body() userCredentialsDto: UserCredentialsDto,
  ): Promise<{ accessToken }> {
    return this.authService.signIn(userCredentialsDto);
  }

  @Post('/test')
  @UseGuards(JwtAuthGuard)
  test(@Req() req) {
    console.log(req);
  }

  @Get('testauth')
  @UseGuards(AuthGuard('jwt'))
  testauth() {
    return 'Test Auth';
  }

}
