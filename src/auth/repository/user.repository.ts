import { DataSource, Repository } from "typeorm";
import { User } from "../entities/user.entity";
import { UserCredentialsDto } from "../dto/user-credentials.dto";
import * as bcrypt from 'bcrypt';
import { ConflictException, Injectable, InternalServerErrorException } from "@nestjs/common";

@Injectable()
export class UserRepository extends Repository<User> {
    constructor(private dataSource: DataSource) {
        super(User, dataSource.createEntityManager());
    }

    async createUser(userCredentialDto: UserCredentialsDto): Promise<void> {
        const { username, password } = userCredentialDto;
    
        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(password, salt);
    
        const user = this.create({
          username,
          password: hashedPassword,
        });
    
        try {
          await this.save(user);
        } catch (error) {
          if (error.code === '23505') {
            //unique error code returning from pgsql driver
            throw new ConflictException('Username already exists');
          } else {
            throw new InternalServerErrorException();
          }
          //console.log(error);
        }
      }
}