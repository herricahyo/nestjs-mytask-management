import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { User } from '../entities/user.entity';
import { JwtPayload } from '../interface/jwt-payload.interface';
import { UserRepository } from '../repository/user.repository';
import { jwtConstants } from 'src/helper/constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly userRepository: UserRepository
  ) {
    super({
      secretOrKey: jwtConstants.secret,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken,
      ignoreExpiration: false,
    });
  }

  // overide function, function ini akan dijalankan jika token berhasil di authentikasi
  async validate(payload: JwtPayload): Promise<User> {
    console.log('jwt strategy validate');
    const { username } = payload;
    const user: User = await this.userRepository.findOneBy({ username });
    console.log(user);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user; //nest passport akan melakukan append variabel user ke setiap request
  }
}
